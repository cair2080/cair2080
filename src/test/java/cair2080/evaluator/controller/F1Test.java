package cair2080.evaluator.controller;

import cair2080.evaluator.exception.DuplicateIntrebareException;
import cair2080.evaluator.exception.InputValidationFailedException;
import cair2080.evaluator.model.Intrebare;
import org.junit.Before;
import org.junit.Test;

public class F1Test {
    private TestController testController;

    @Before
    public void initializeParameters() {
        testController = TestUtils.getInitializedController();
    }

    @Test
    public void addNewIntrebare_TC1_ECP_Test() throws InputValidationFailedException, DuplicateIntrebareException {
        String enunt = "Intrebare?";
        String varianta1 = "1)Da";
        String varianta2 = "2)Nu";
        String varianta3 = "3)Poate";
        String raspunsCorect = "2";
        String domeniu = "Fizica";
        Intrebare intrebare = new Intrebare(enunt, varianta1, varianta2, varianta3, raspunsCorect, domeniu);
        testController.addNewIntrebare(intrebare);
    }

    @Test(expected = InputValidationFailedException.class)
    public void addNewIntrebare_TC2_ECP_Test() throws InputValidationFailedException, DuplicateIntrebareException {
        StringBuilder enunt = new StringBuilder();
        enunt.append("A");
        for (int i = 0; i < 99; i++) {
            enunt.append("a");
        }
        enunt.append("?");
        String varianta1 = "1)Da";
        String varianta2 = "2)Nu";
        String varianta3 = "3)Poate";
        String raspunsCorect = "2";
        String domeniu = "Informatica";
        Intrebare intrebare = new Intrebare(enunt.toString(), varianta1, varianta2, varianta3, raspunsCorect, domeniu);
        testController.addNewIntrebare(intrebare);
    }

    @Test(expected = InputValidationFailedException.class)
    public void addNewIntrebare_TC3_ECP_Test() throws InputValidationFailedException, DuplicateIntrebareException {
        String enunt = "";
        String varianta1 = "1)Da";
        String varianta2 = "2)Nu";
        String varianta3 = "3)Poate";
        String raspunsCorect = "2";
        String domeniu = "Fizica";
        Intrebare intrebare = new Intrebare(enunt, varianta1, varianta2, varianta3, raspunsCorect, domeniu);
        testController.addNewIntrebare(intrebare);
    }

    @Test(expected = InputValidationFailedException.class)
    public void addNewIntrebare_TC5_ECP_Test() throws InputValidationFailedException, DuplicateIntrebareException {
        String enunt = "Intrebare?";
        StringBuilder varianta1 = new StringBuilder("1)");
        for (int i = 0; i < 49; i++) {
            varianta1.append("a");
        }
        String varianta2 = "2)Nu";
        String varianta3 = "3)Poate";
        String raspunsCorect = "2";
        String domeniu = "Engleza";
        Intrebare intrebare = new Intrebare(enunt, varianta1.toString(), varianta2, varianta3, raspunsCorect, domeniu);
        testController.addNewIntrebare(intrebare);
    }

    @Test(expected = InputValidationFailedException.class)
    public void addNewIntrebare_TC6_ECP_Test() throws InputValidationFailedException, DuplicateIntrebareException {
        String enunt = "Intrebare?";
        String varianta1 = "";
        String varianta2 = "2)Nu";
        String varianta3 = "3)Poate";
        String raspunsCorect = "2";
        String domeniu = "Biologie";
        Intrebare intrebare = new Intrebare(enunt, varianta1, varianta2, varianta3, raspunsCorect, domeniu);
        testController.addNewIntrebare(intrebare);
    }

    @Test(expected = InputValidationFailedException.class)
    public void addNewIntrebare_TC3_BVA_Test() throws InputValidationFailedException, DuplicateIntrebareException {
        String enunt = "?";
        String varianta1 = "1)Da";
        String varianta2 = "2)Nu";
        String varianta3 = "3)Poate";
        String raspunsCorect = "2";
        String domeniu = "Fizica";
        Intrebare intrebare = new Intrebare(enunt, varianta1, varianta2, varianta3, raspunsCorect, domeniu);
        testController.addNewIntrebare(intrebare);
    }

    @Test
    public void addNewIntrebare_TC4_BVA_Test() throws InputValidationFailedException, DuplicateIntrebareException {
        StringBuilder enunt = new StringBuilder();
        for (int i = 0; i < 99; i++) {
            enunt.append("E");
        }
        enunt.append("?");
        String varianta1 = "1)Da";
        String varianta2 = "2)Nu";
        String varianta3 = "3)Poate";
        String raspunsCorect = "2";
        String domeniu = "Fizica";
        Intrebare intrebare = new Intrebare(enunt.toString(), varianta1, varianta2, varianta3, raspunsCorect, domeniu);
        testController.addNewIntrebare(intrebare);
    }

    @Test
    public void addNewIntrebare_TC5_BVA_Test() throws InputValidationFailedException, DuplicateIntrebareException {
        StringBuilder enunt = new StringBuilder();
        for (int i = 0; i < 98; i++) {
            enunt.append("E");
        }
        enunt.append("?");
        String varianta1 = "1)Da";
        String varianta2 = "2)Nu";
        String varianta3 = "3)Poate";
        String raspunsCorect = "2";
        String domeniu = "Fizica";
        Intrebare intrebare = new Intrebare(enunt.toString(), varianta1, varianta2, varianta3, raspunsCorect, domeniu);
        testController.addNewIntrebare(intrebare);
    }

    @Test(expected = InputValidationFailedException.class)
    public void addNewIntrebare_TC9_BVA_Test() throws InputValidationFailedException, DuplicateIntrebareException {
        String enunt = "E?";
        String varianta1 = "1";
        String varianta2 = "2)Nu";
        String varianta3 = "3)Poate";
        String raspunsCorect = "2";
        String domeniu = "Fizica";
        Intrebare intrebare = new Intrebare(enunt, varianta1, varianta2, varianta3, raspunsCorect, domeniu);
        testController.addNewIntrebare(intrebare);
    }

    @Test
    public void addNewIntrebare_TC10_BVA_Test() throws InputValidationFailedException, DuplicateIntrebareException {
        String enunt = "E?";
        StringBuilder varianta1 = new StringBuilder("1)");
        for (int i = 0; i < 48; i++) {
            varianta1.append("V");
        }
        String varianta2 = "2)Nu";
        String varianta3 = "3)Poate";
        String raspunsCorect = "2";
        String domeniu = "Fizica";
        Intrebare intrebare = new Intrebare(enunt, varianta1.toString(), varianta2, varianta3, raspunsCorect, domeniu);
        testController.addNewIntrebare(intrebare);
    }

    @Test
    public void addNewIntrebare_TC11_BVA_Test() throws InputValidationFailedException, DuplicateIntrebareException {
        String enunt = "E?";
        StringBuilder varianta1 = new StringBuilder("1)");
        for (int i = 0; i < 47; i++) {
            varianta1.append("V");
        }
        String varianta2 = "2)Nu";
        String varianta3 = "3)Poate";
        String raspunsCorect = "2";
        String domeniu = "Fizica";
        Intrebare intrebare = new Intrebare(enunt, varianta1.toString(), varianta2, varianta3, raspunsCorect, domeniu);
        testController.addNewIntrebare(intrebare);
    }
}
