package cair2080.evaluator.controller;

import cair2080.evaluator.exception.DuplicateIntrebareException;
import cair2080.evaluator.exception.InputValidationFailedException;
import cair2080.evaluator.exception.NotAbleToCreateStatisticsException;
import cair2080.evaluator.exception.NotAbleToCreateTestException;
import cair2080.evaluator.model.Intrebare;
import cair2080.evaluator.model.Statistica;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class IntegrareTopDownTest {
    private TestController testController;
    private List<Intrebare> intrebari;

    @Before
    public void initializeParameters() throws InputValidationFailedException {
        testController = TestUtils.getInitializedController();
        intrebari = TestUtils.getInitializedIntrebariList();
    }

    @Test
    public void unitar_F01_Test() throws InputValidationFailedException, DuplicateIntrebareException {
        String enunt = "Intrebare?";
        String varianta1 = "1)Da";
        String varianta2 = "2)Nu";
        String varianta3 = "3)Poate";
        String raspunsCorect = "2";
        String domeniu = "Fizica";
        Intrebare intrebare = new Intrebare(enunt, varianta1, varianta2, varianta3, raspunsCorect, domeniu);
        testController.addNewIntrebare(intrebare);
    }

    @Test
    public void unitar_F02_Test() throws DuplicateIntrebareException, NotAbleToCreateTestException {
        testController.addNewIntrebare(intrebari.get(0));
        testController.addNewIntrebare(intrebari.get(1));
        testController.addNewIntrebare(intrebari.get(2));
        testController.addNewIntrebare(intrebari.get(3));
        testController.addNewIntrebare(intrebari.get(4));

        cair2080.evaluator.model.Test test = testController.createNewTest();

        assertEquals(5, test.getIntrebari().size());
        assertTrue(test.getIntrebari().contains(intrebari.get(0)));
        assertTrue(test.getIntrebari().contains(intrebari.get(1)));
        assertTrue(test.getIntrebari().contains(intrebari.get(2)));
        assertTrue(test.getIntrebari().contains(intrebari.get(3)));
        assertTrue(test.getIntrebari().contains(intrebari.get(4)));
    }

    @Test
    public void unitar_F03_Test() throws NotAbleToCreateStatisticsException, DuplicateIntrebareException {
        testController.addNewIntrebare(intrebari.get(0));
        testController.addNewIntrebare(intrebari.get(1));
        testController.addNewIntrebare(intrebari.get(2));
        testController.addNewIntrebare(intrebari.get(5));
        testController.addNewIntrebare(intrebari.get(6));

        Statistica statistica = testController.getStatistica();
        Map<String, Integer> intrebariDomenii = statistica.getIntrebariDomenii();

        assertEquals(3, intrebariDomenii.get("Matematica"), 0.0);
        assertEquals(1, intrebariDomenii.get("Informatica"), 0.0);
        assertEquals(1, intrebariDomenii.get("Fizica"), 0.0);
        assertEquals(3, intrebariDomenii.size());
    }

    @Test
    public void integrare_P_F01_Test() throws DuplicateIntrebareException, InputValidationFailedException {
        String enunt = "Intrebare?";
        String varianta1 = "1)Da";
        String varianta2 = "2)Nu";
        String varianta3 = "3)Poate";
        String raspunsCorect = "2";
        String domeniu = "Fizica";
        Intrebare intrebare = new Intrebare(enunt, varianta1, varianta2, varianta3, raspunsCorect, domeniu);
        testController.addNewIntrebare(intrebare);
        testController.exists(intrebare);

        testController.addNewIntrebare(intrebari.get(0));
        testController.addNewIntrebare(intrebari.get(1));
        testController.addNewIntrebare(intrebari.get(3));
        testController.addNewIntrebare(intrebari.get(4));
        testController.exists(intrebari.get(0));
        testController.exists(intrebari.get(1));
        testController.exists(intrebari.get(3));
        testController.exists(intrebari.get(4));
    }

    @Test
    public void integrare_P_F01_F02_Test() throws NotAbleToCreateTestException, DuplicateIntrebareException, InputValidationFailedException {
        String enunt = "Intrebare?";
        String varianta1 = "1)Da";
        String varianta2 = "2)Nu";
        String varianta3 = "3)Poate";
        String raspunsCorect = "2";
        String domeniu = "Fizica";
        Intrebare intrebare = new Intrebare(enunt, varianta1, varianta2, varianta3, raspunsCorect, domeniu);
        testController.addNewIntrebare(intrebare);
        testController.exists(intrebare);

        testController.addNewIntrebare(intrebari.get(0));
        testController.addNewIntrebare(intrebari.get(1));
        testController.addNewIntrebare(intrebari.get(3));
        testController.addNewIntrebare(intrebari.get(4));
        testController.exists(intrebari.get(0));
        testController.exists(intrebari.get(1));
        testController.exists(intrebari.get(3));
        testController.exists(intrebari.get(4));

        cair2080.evaluator.model.Test test = testController.createNewTest();
        assertEquals(5, test.getIntrebari().size());

        assertTrue(test.getIntrebari().contains(intrebare));
        assertTrue(test.getIntrebari().contains(intrebari.get(0)));
        assertTrue(test.getIntrebari().contains(intrebari.get(1)));
        assertTrue(test.getIntrebari().contains(intrebari.get(3)));
        assertTrue(test.getIntrebari().contains(intrebari.get(4)));
    }

    @Test
    public void integrare_P_F01_F02_F03_Test() throws NotAbleToCreateStatisticsException, NotAbleToCreateTestException, DuplicateIntrebareException, InputValidationFailedException {
        String enunt = "Intrebare?";
        String varianta1 = "1)Da";
        String varianta2 = "2)Nu";
        String varianta3 = "3)Poate";
        String raspunsCorect = "2";
        String domeniu = "Fizica";
        Intrebare intrebare = new Intrebare(enunt, varianta1, varianta2, varianta3, raspunsCorect, domeniu);
        testController.addNewIntrebare(intrebare);
        testController.exists(intrebare);

        testController.addNewIntrebare(intrebari.get(0));
        testController.addNewIntrebare(intrebari.get(1));
        testController.addNewIntrebare(intrebari.get(3));
        testController.addNewIntrebare(intrebari.get(4));
        testController.exists(intrebari.get(0));
        testController.exists(intrebari.get(1));
        testController.exists(intrebari.get(3));
        testController.exists(intrebari.get(4));

        cair2080.evaluator.model.Test test = testController.createNewTest();
        assertEquals(5, test.getIntrebari().size());

        assertTrue(test.getIntrebari().contains(intrebare));
        assertTrue(test.getIntrebari().contains(intrebari.get(0)));
        assertTrue(test.getIntrebari().contains(intrebari.get(1)));
        assertTrue(test.getIntrebari().contains(intrebari.get(3)));
        assertTrue(test.getIntrebari().contains(intrebari.get(4)));

        Statistica statistica = testController.getStatistica();
        Map<String, Integer> intrebariDomenii = statistica.getIntrebariDomenii();
        assertEquals(5, intrebariDomenii.size());

        assertEquals(1, intrebariDomenii.get("Matematica"), 0.0);
        assertEquals(1, intrebariDomenii.get("Informatica"), 0.0);
        assertEquals(1, intrebariDomenii.get("Fizica"), 0.0);
        assertEquals(1, intrebariDomenii.get("Biologie"), 0.0);
        assertEquals(1, intrebariDomenii.get("Romana"), 0.0);
    }
}
