package cair2080.evaluator.controller;

import cair2080.evaluator.exception.DuplicateIntrebareException;
import cair2080.evaluator.exception.InputValidationFailedException;
import cair2080.evaluator.exception.NotAbleToCreateTestException;
import cair2080.evaluator.model.Intrebare;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class F2Test {
    private TestController testController;
    private List<Intrebare> intrebari;

    @Before
    public void initializeParameters() throws InputValidationFailedException {
        testController = TestUtils.getInitializedController();
        intrebari = TestUtils.getInitializedIntrebariList();
    }

    @Test(expected = NotAbleToCreateTestException.class)
    public void createNewTest_F02_TC01_TC12_Test() throws DuplicateIntrebareException, NotAbleToCreateTestException {
        testController.addNewIntrebare(intrebari.get(0));
        testController.createNewTest();
    }

    @Test(expected = NotAbleToCreateTestException.class)
    public void createNewTest_F02_TC02_TC13_Test() throws DuplicateIntrebareException, NotAbleToCreateTestException {
        testController.addNewIntrebare(intrebari.get(0));
        testController.addNewIntrebare(intrebari.get(1));
        testController.addNewIntrebare(intrebari.get(2));
        testController.addNewIntrebare(intrebari.get(5));
        testController.addNewIntrebare(intrebari.get(6));
        testController.createNewTest();
    }

    @Test
    public void createNewTest_F02_TC01_TC14_Test() throws DuplicateIntrebareException, NotAbleToCreateTestException {
        testController.addNewIntrebare(intrebari.get(0));
        testController.addNewIntrebare(intrebari.get(1));
        testController.addNewIntrebare(intrebari.get(2));
        testController.addNewIntrebare(intrebari.get(3));
        testController.addNewIntrebare(intrebari.get(4));

        cair2080.evaluator.model.Test test = testController.createNewTest();

        assertEquals(5, test.getIntrebari().size());
        assertTrue(test.getIntrebari().contains(intrebari.get(0)));
        assertTrue(test.getIntrebari().contains(intrebari.get(1)));
        assertTrue(test.getIntrebari().contains(intrebari.get(2)));
        assertTrue(test.getIntrebari().contains(intrebari.get(3)));
        assertTrue(test.getIntrebari().contains(intrebari.get(4)));
    }

    @Test
    public void createNewTest_F02_TC01_TC15_Test() throws DuplicateIntrebareException, NotAbleToCreateTestException {
        testController.addNewIntrebare(intrebari.get(0));
        testController.addNewIntrebare(intrebari.get(1));
        testController.addNewIntrebare(intrebari.get(2));
        testController.addNewIntrebare(intrebari.get(3));
        testController.addNewIntrebare(intrebari.get(4));
        testController.addNewIntrebare(intrebari.get(5));
        testController.addNewIntrebare(intrebari.get(6));
        testController.addNewIntrebare(intrebari.get(7));
        testController.addNewIntrebare(intrebari.get(8));
        testController.addNewIntrebare(intrebari.get(9));

        cair2080.evaluator.model.Test test = testController.createNewTest();

        assertEquals(5, test.getIntrebari().size());
        assertTrue(test.getIntrebari().contains(intrebari.get(0)) || test.getIntrebari().contains(intrebari.get(5))
                || test.getIntrebari().contains(intrebari.get(6)) || test.getIntrebari().contains(intrebari.get(7))
                || test.getIntrebari().contains(intrebari.get(8)) || test.getIntrebari().contains(intrebari.get(9)));
        assertTrue(test.getIntrebari().contains(intrebari.get(1)));
        assertTrue(test.getIntrebari().contains(intrebari.get(2)));
        assertTrue(test.getIntrebari().contains(intrebari.get(3)));
        assertTrue(test.getIntrebari().contains(intrebari.get(4)));
    }
}
