package cair2080.evaluator.controller;

import cair2080.evaluator.exception.DuplicateIntrebareException;
import cair2080.evaluator.exception.InputValidationFailedException;
import cair2080.evaluator.exception.NotAbleToCreateStatisticsException;
import cair2080.evaluator.model.Intrebare;
import cair2080.evaluator.model.Statistica;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class F3Test {
    private TestController testController;
    private List<Intrebare> intrebari;

    @Before
    public void initializeParameters() throws InputValidationFailedException {
        testController = TestUtils.getInitializedController();
        intrebari = TestUtils.getInitializedIntrebariList();
    }

    @Test(expected = NotAbleToCreateStatisticsException.class)
    public void getStatistica_001_Test() throws NotAbleToCreateStatisticsException {
        testController.getStatistica();
    }

    @Test
    public void getStatistica_002_Test() throws NotAbleToCreateStatisticsException, DuplicateIntrebareException {
        testController.addNewIntrebare(intrebari.get(0));
        testController.addNewIntrebare(intrebari.get(1));
        testController.addNewIntrebare(intrebari.get(2));
        testController.addNewIntrebare(intrebari.get(5));
        testController.addNewIntrebare(intrebari.get(6));

        Statistica statistica = testController.getStatistica();
        Map<String, Integer> intrebariDomenii = statistica.getIntrebariDomenii();

        assertEquals(3, intrebariDomenii.get("Matematica"), 0.0);
        assertEquals(1, intrebariDomenii.get("Informatica"), 0.0);
        assertEquals(1, intrebariDomenii.get("Fizica"), 0.0);
        assertEquals(3, intrebariDomenii.size());
    }

    @Test
    public void getStatistica_003_Test() throws NotAbleToCreateStatisticsException, DuplicateIntrebareException {
        testController.addNewIntrebare(intrebari.get(0));
        testController.addNewIntrebare(intrebari.get(1));
        testController.addNewIntrebare(intrebari.get(2));
        testController.addNewIntrebare(intrebari.get(3));
        testController.addNewIntrebare(intrebari.get(4));
        testController.addNewIntrebare(intrebari.get(5));
        testController.addNewIntrebare(intrebari.get(6));
        testController.addNewIntrebare(intrebari.get(7));
        testController.addNewIntrebare(intrebari.get(8));
        testController.addNewIntrebare(intrebari.get(9));

        Statistica statistica = testController.getStatistica();
        Map<String, Integer> intrebariDomenii = statistica.getIntrebariDomenii();

        assertEquals(6, intrebariDomenii.get("Matematica"), 0.0);
        assertEquals(1, intrebariDomenii.get("Informatica"), 0.0);
        assertEquals(1, intrebariDomenii.get("Fizica"), 0.0);
        assertEquals(1, intrebariDomenii.get("Romana"), 0.0);
        assertEquals(1, intrebariDomenii.get("Biologie"), 0.0);
        assertEquals(5, intrebariDomenii.size());
    }
}
