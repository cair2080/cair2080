package cair2080.evaluator.controller;

import cair2080.evaluator.exception.InputValidationFailedException;
import cair2080.evaluator.model.Intrebare;

import java.util.ArrayList;
import java.util.List;

public class TestUtils {
    public static TestController getInitializedController() {
        return new TestController();
    }

    public static List<Intrebare> getInitializedIntrebariList() throws InputValidationFailedException {
        List<Intrebare> intrebari = new ArrayList<>();
        intrebari.add(new Intrebare("I0?", "1)v1", "2)v2", "3)v3", "1", "Matematica"));
        intrebari.add(new Intrebare("I1?", "1)v1", "2)v2", "3)v3", "1", "Informatica"));
        intrebari.add(new Intrebare("I2?", "1)v1", "2)v2", "3)v3", "1", "Fizica"));
        intrebari.add(new Intrebare("I3?", "1)v1", "2)v2", "3)v3", "1", "Romana"));
        intrebari.add(new Intrebare("I4?", "1)v1", "2)v2", "3)v3", "1", "Biologie"));
        intrebari.add(new Intrebare("I5?", "1)v1", "2)v2", "3)v3", "1", "Matematica"));
        intrebari.add(new Intrebare("I6?", "1)v1", "2)v2", "3)v3", "1", "Matematica"));
        intrebari.add(new Intrebare("I7?", "1)v1", "2)v2", "3)v3", "1", "Matematica"));
        intrebari.add(new Intrebare("I8?", "1)v1", "2)v2", "3)v3", "1", "Matematica"));
        intrebari.add(new Intrebare("I9?", "1)v1", "2)v2", "3)v3", "1", "Matematica"));
        return intrebari;
    }
}
