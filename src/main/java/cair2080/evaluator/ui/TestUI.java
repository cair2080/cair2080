package cair2080.evaluator.ui;

import cair2080.evaluator.controller.TestController;
import cair2080.evaluator.exception.DuplicateIntrebareException;
import cair2080.evaluator.exception.InputValidationFailedException;
import cair2080.evaluator.exception.NotAbleToCreateStatisticsException;
import cair2080.evaluator.exception.NotAbleToCreateTestException;
import cair2080.evaluator.model.Intrebare;
import cair2080.evaluator.model.Statistica;
import cair2080.evaluator.model.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class TestUI {
    private String file;
    private TestController testController;

    public TestUI(String file, TestController testController) {
        this.file = file;
        this.testController = testController;
    }

    private void adaugaIntrebare() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enunt/Intrebare: ");
        String enunt = scanner.nextLine();

        System.out.println("Varianta 1 de raspuns: ");
        String varianta1 = scanner.nextLine();

        System.out.println("Varianta 2 de raspuns: ");
        String varianta2 = scanner.nextLine();

        System.out.println("Varianta 3 de raspuns: ");
        String varianta3 = scanner.nextLine();

        System.out.println("Varianta corecta de raspuns: ");
        String variantaCorecta = scanner.nextLine();

        System.out.println("Domeniul: ");
        String domeniu = scanner.nextLine();

        try {
            Intrebare intrebare = new Intrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
            testController.addNewIntrebare(intrebare);
        } catch (InputValidationFailedException | DuplicateIntrebareException e) {
            System.out.println(e.getMessage());
        }
    }

    private void creareTest() {
        try {
            Test test = testController.createNewTest();
            test.getIntrebari().forEach(System.out::println);
        } catch (NotAbleToCreateTestException e) {
            System.out.println(e.getMessage());
        }
    }

    public void run() throws IOException {
        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

        boolean activ = true;
        String optiune = null;

        while (activ) {

            System.out.println("");
            System.out.println("1.Adauga intrebare");
            System.out.println("2.Creeaza test");
            System.out.println("3.Statistica");
            System.out.println("4.Exit");
            System.out.println("");

            optiune = console.readLine();

            switch (optiune) {
                case "1":
                    adaugaIntrebare();
                    break;
                case "2":
                    creareTest();
                    break;
                case "3":
                    testController.loadIntrebariFromFile(file);
                    Statistica statistica;
                    try {
                        statistica = testController.getStatistica();
                        System.out.println(statistica);
                    } catch (NotAbleToCreateStatisticsException e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case "4":
                    activ = false;
                    break;
                default:
                    break;
            }
        }
    }
}
